#!/usr/bin/env node

function parseChanges(data){
  var changes = [];
  var rawChanges = data.split('\n');
  rawChanges = rawChanges.filter(function(item){
    var gerritStuff = item.indexOf('"type":"stats"') > -1;
    var emptyString = item.length === 0;
    return !(gerritStuff || emptyString);
  });

  changes = rawChanges.map(JSON.parse);
  changes = changes.sort(changesComparator);

  console.log(JSON.stringify(changes));

}

function changesComparator(a,b){
  var na = parseInt(a.currentPatchSet.createdOn, 10);
  var nb = parseInt(b.currentPatchSet.createdOn, 10);
  if (a.currentPatchSet.parents.indexOf(b.currentPatchSet.revision) != -1) {
    return -1;
  } else if (b.currentPatchSet.parents.indexOf(a.currentPatchSet.revision) != -1) {
    return 1;
  } else if (na > nb) {
    return 1;
  } else if (na < nb) {
    return -1;
  } else if (a.sortKey > b.sortKey ) {
    return 1;
  } else if (a.sortKey < b.sortKey ) {
    return -1;
  } else {
    return 0;
  }
}

parseChanges(process.argv[2])
