# cleandev flow control module
# read input, print help, usage

function USAGE(){
  echo "cleandev - build changes from gerrit without merging them"
  echo "USAGE:"
  echo "$0 -w] <workpsace> [[--queue | -q ] <build queue>] [[--build-options | -o ] <build options>]"
  echo "  <workspace> [REQUIRED] workspace settings name"
  echo "  <build queue> [OPTIONAL] list of projects to build. Have to be list of
  space sepearated project keys and contained in double qutes.
  Example: -o \"first-project \"
  If empty, \"queue\" from <workspace> file will be used"
  exit 1;
}
function USAGE(){
  echo "cleandev - build changes from gerrit without merging them
  USAGE:
  $0 [--workspace | -w] <workspace> [OPTIONS]

  To list available workspaces use $0 --workspace without secon argument
  OPTIONS

  --queue, -q LIST
    This option overrides build queue from config file with LIST.
    LIST is a string containing space separated project keys, i.e:
    --queue \"first-project second-project third-project\"

  --build-options, -o OPTIONS
    Add OPTIONS to build command for every project being built.
    OPTIONS is a string.

  "
  echo
  echo "Use -h or --help to display full help"
  exit 1;
}

if [ $# = 0 ]; then
    USAGE_SHORT
fi

while [[ $# > 0 ]]; do
    case $1 in
        --workspace | -w)
            WORKSPACE=$2
            shift
            shift
        ;;
        --queue | -q)
            CLI_QUEUE=$2
            shift
            shift
        ;;
        --build-options | -o)
            CLI_BUILD_OPTIONS=$2
            shift
            shift
        ;;
        *)
            USAGE
            shift
        ;;
    esac
done
