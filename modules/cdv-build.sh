#!/usr/bin/env bash

#use additional options from command line (paremeter --build-options | -o)
if [ $# -ne 0 ] && [ ! -z "$1" ];then
  PROJECT_BUILD_COMMAND="$PROJECT_BUILD_COMMAND $1";
fi

#go to project directory and execute build command
cd $WORKSPACE_PATH/$PROJECT_LOCAL_PATH;
[ $? == 0 ] || exit 1;

echo "Execute $PROJECT_BUILD_COMMAND"
echo
$PROJECT_BUILD_COMMAND
[ $? == 0 ] || exit 1;
