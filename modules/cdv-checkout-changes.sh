#!/usr/bin/env bash

#read list of changes and cherry-pick them one by one
cd $WORKSPACE_PATH/$PROJECT_LOCAL_PATH;
[ $? == 0 ] || exit 1;

for CHANGE in  $1;
do
  [[ ! -d .git/rebase-gerrit ]] || rm -rf .git/rebase-gerrit
  [[ ! -d .git/rebase-apply ]] || rm -rf .git/rebase-apply
  gerrit-cherry-pick $GERRIT_REMOTE $CHANGE
  [ $? == 0 ] || exit 1;
done
