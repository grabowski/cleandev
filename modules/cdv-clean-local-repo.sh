#!/usr/bin/env bash

#reset local repo to given branch
#fetch origin repo and rebase to remote branch

cd $WORKSPACE_PATH/$PROJECT_LOCAL_PATH;
pwd
echo "$(git status --porcelain)"
if [ ! -z "$(git status --porcelain)" ];then
  echo "Index dirty."
  echo "Removing untracked files and directories."
  git clean -f -d;
  echo "Stashing local changes if any and reset to HEAD";
  git stash -q;
  git reset --hard;
  echo "Aborting rebase if any";
  git rebase --abort;
  echo "Aborting cherry-pick if any";
  git cherry-pick --abort;
fi
echo "Index clean."
echo "Fetching and rebasing to $GERRIT_REMOTE/$PROJECT_GERRIT_BRANCH"
git fetch $GERRIT_REMOTE -q;

git checkout $PROJECT_GERRIT_BRANCH -q;
git reset --hard $GERRIT_REMOTE/$PROJECT_GERRIT_BRANCH ;
