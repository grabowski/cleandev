#!/usr/bin/env bash

#clone project to specified local path if not present
if [ -d $WORKSPACE_PATH/$PROJECT_LOCAL_PATH ];then
  exit 0
else
  echo "Project not in workspace. Cloning from $GERRIT_SERVER/$PROJECT_GERRIT_PATH";
  [ -d $WORKSPACE_PATH ] || mkdir -p $WORKSPACE_PATH;
  [ $? == 0 ] || exit 1;
  git clone ssh://$GERRIT_USER@$GERRIT_SERVER/$PROJECT_GERRIT_PATH $WORKSPACE_PATH/$PROJECT_LOCAL_PATH;
fi
