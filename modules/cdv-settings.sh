#!/usr/bin/env bash

DEFAULT_WORKSPACE_DIR=$CDV_HOME/settings;
# read .cleandevrc file from users profile
if [ -e ~/.cleandevrc ];then
  source ~/.cleandevrc
fi

[ -n "$CLEANDEV_SETTINGS_DIR" ] || CLEANDEV_SETTINGS_DIR=$DEFAULT_WORKSPACE_DIR

# helper function to get value from configuration JSON
# $1 configuration (JSON string)
# $2 key
function getConfigValue(){
  echo $1 | `echo "jq --raw-output .$2"`
}

# parse JSON with project settings and export them as variables
# $1 workspace settings name
# $2 project key
function parse_project_settings(){
  local project=`echo $2 | sed 's/"//g'`
  local config=`cat $CLEANDEV_SETTINGS_DIR/$1.json`;
  local projects=`echo $config | jq --raw-output '.projects | keys[]'`;
  local project_configured=`echo "$projects" | grep $project`;
  if [ -z $project_configured ];then
    echo "[CLEANDEV][ERROR][PROJECT NOT CONFIGURED]"
    echo "$project: no such project configured in workspace $1";
    echo "Available projects in this workspace are:";
    printf "%s\n" "$projects"
    exit 1
  fi

  local project_config=`echo $config | \`echo "jq .projects[\"$project\"]"\``;

  export PROJECT_LOCAL_PATH=`getConfigValue "$project_config" "localPath"`;
  export PROJECT_GERRIT_PATH=`getConfigValue "$project_config" "gerritPath"`;
  export PROJECT_GERRIT_BRANCH=`getConfigValue "$project_config" "gerritBranch"`;
  export PROJECT_BUILD_COMMAND=`getConfigValue "$project_config" "buildCommand"`;
  export PROJECT_ALWAYS_BUILD=`getConfigValue "$project_config" "alwaysBuild"`;
}

# parse JSON with workspace settings and export them as variables
# $1 workspace settings name
function parse_workspace_settings(){
  if [ -z $1 ];then
    echo "Available workspace configs are:";
    ls -A $CLEANDEV_SETTINGS_DIR | grep .json | sed 's/.json//g'
    echo "Config files available in :$CLEANDEV_SETTINGS_DIR";
    exit 1
  fi

  if [ ! -f "$CLEANDEV_SETTINGS_DIR/$1.json" ];then
    echo "[CLEANDEV][ERROR][WORKSPACE NOT CONFIGURED]"
    echo "[ERROR] $1: no such workspace is configured";
    echo "Available workspaces are:";
    ls -A $CLEANDEV_SETTINGS_DIR | grep .json | sed 's/.json//g'
    exit 1
  fi

  local config=`cat $CLEANDEV_SETTINGS_DIR/$1.json`;
  export GERRIT_SERVER=`getConfigValue "$config" "gerrit.server"`;
  export GERRIT_USER=`getConfigValue "$config" "gerrit.user"`;
  export GERRIT_REMOTE=`getConfigValue "$config" "gerrit.remote"`;
  export GERRIT_FILTER_STATUS=`getConfigValue "$config" "gerrit.filter.status"`;
  export GERRIT_FILTER_CR_ALLOW=`getConfigValue "$config" "gerrit.filter.codeReview.allow"`;
  export GERRIT_FILTER_CR_DENY=`getConfigValue "$config" "gerrit.filter.codeReview.deny"`;

  export WORKSPACE_PATH=`getConfigValue "$config" "workspace.path"`;
  export WORKSPACE_QUEUE=`echo $config | jq '.queue[]'`;
}
