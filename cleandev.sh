#!/usr/bin/env bash

#1 read project list (from file or from args) | workspace is an argument
#2 for every project in this list do
# fetch changes list if none, exit, if some or project is always built do
#  workspace (ensure that project is cloned to given path)
#  clean repo
#  cherry pick changes one by one
#  build project with its command

export CDV_HOME=$(dirname $0)
export CDV_MODULES=$CDV_HOME/modules;
source $CDV_MODULES/cdv-settings.sh
source $CDV_MODULES/cdv-flow-control.sh

parse_workspace_settings $WORKSPACE
if [ -z "$CLI_QUEUE" ];then
  QUEUE=$WORKSPACE_QUEUE;
else
  QUEUE=$CLI_QUEUE;
fi


if [ ${#QUEUE[@]} -gt 0 ]; then
  for PROJECT in $QUEUE;do
      echo
      echo "[CLEANDEV][$PROJECT]"
      parse_project_settings $WORKSPACE $PROJECT
      [ $? == 0 ] || exit 1;

      CHANGES=`$CDV_MODULES/cdv-changes-list.sh`
      [ $? == 0 ] || exit 1;

      if [ ${#CHANGES} -ne 0 ] || $PROJECT_ALWAYS_BUILD; then
        if [ ${#CHANGES} -eq 0 ] && $PROJECT_ALWAYS_BUILD;then
          echo "No changes found. Permanent build enabled. Building anyway"
        fi

        echo "[CLEANDEV][$PROJECT][WORKSPACE]"
        $CDV_MODULES/cdv-workspace.sh
        [ $? == 0 ] || exit 1;

        echo "[CLEANDEV][$PROJECT][GIT-REBASE]"
        $CDV_MODULES/cdv-clean-local-repo.sh
        [ $? == 0 ] || exit 1;

        if [ ${#CHANGES} -ne 0 ];then
          echo "[CLEANDEV][$PROJECT][GERRIT-CHANGES]"
          $CDV_MODULES/cdv-checkout-changes.sh "${CHANGES[@]}"
          [ $? == 0 ] || exit 1;
        fi

        echo "[CLEANDEV][$PROJECT][BUILD]"
        $CDV_MODULES/cdv-build.sh "$CLI_BUILD_OPTIONS"
        [ $? == 0 ] || exit 1;
      else
        echo "No changes found. Skipping build."
      fi
    done
fi
